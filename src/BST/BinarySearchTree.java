package BST;

/**
 * Created by letsboogey on 13/02/2016.
 */

public class BinarySearchTree<E> {

    //only data field in BST, the root node
    public Node<E> root;

    //creates new empty BST
    public BinarySearchTree(){
        root = null;
    }

    //find an item
    public Node<E> find(int key){
        //check if tree is empty(root = null)
        if(root == null){
            System.out.println("Tree is empty");
            return null;
        }

        Node current = root;

        //search for node containing key
        while(current.key != key) {
            current = (key < current.key) ? current.left_child : current.right_child;
            //return null if key not found
            if(current == null) {
                System.out.println("Key id: "+key+" not found");
                return null;
            }
        }
        //return node with target key
        return current;
    }

    //insert an item
    public void insert(int key, E data){
        //create the node to be inserted
        Node newNode = new Node();
        newNode.key = key;
        newNode.data = data;

        //insert as root if tree is empty
        if(root == null) {
            root = newNode;
            newNode.displayNode();
            System.out.println("Inserted at ROOT");//for testing purposes

        }else{//find suitable position to insert
            Node current = root;//used to traverse through nodes
            Node parent;//used to keep track of current

            //begin search for suitable insert position
            while(true){
                parent = current;
                if(key < current.key){
                    current = current.left_child;
                    if(current == null){//left child is empty therefore we can insert new node on left
                        parent.left_child = newNode;
                        newNode.displayNode();
                        System.out.println("inserted at left of:  "+ parent.key);
                        return;
                    }
                }else{
                    current = current.right_child;
                    if(current == null){//right child is empty therefore we can insert new node on right
                        parent.right_child = newNode;
                        newNode.displayNode();
                        System.out.println("inserted at right of:  "+ parent.key);
                        return;
                    }
                }
            }
        }
    }

    //Traversing the tree

    //inorder(visits in ascending order)
    public void inorder(Node node){
        if(node != null){
            inorder(node.left_child);
            node.displayNode();
            inorder(node.right_child);
        }
    }

    //preorder
    public void preorder(Node node){
        if(node != null){
            node.displayNode();
            preorder(node.left_child);
            preorder(node.right_child);
        }
    }

    //postorder
    public void postorder(Node node){
        if(node != null){
            postorder(node.left_child);
            postorder(node.right_child);
            node.displayNode();
        }
    }

    //finding minimum value
    public Node minimum(){
        Node current, last = root;
        current = root;
        while (current != null){
            last = current;
            current = current.left_child;
        }
        return last;
    }






    class Node<E>{
        public int key;
        public E data;
        public Node<E> left_child;
        public Node<E> right_child;

        public void displayNode(){

            System.out.print("{");
            System.out.print(key);
            System.out.print(", ");
            System.out.print(data);
            System.out.print("}\n");

        }
    }
}
