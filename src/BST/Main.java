package BST;

/**
 * Created by letsboogey on 14/02/2016.
 */
import java.util.Random;
public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();

        Random rand = new Random();
        for(int i=0 ; i<10 ;i++){
            bst.insert(rand.nextInt(100),i%2);
        }

        bst.inorder(bst.root);
        bst.postorder(bst.root);
        bst.preorder(bst.root);
        System.out.println("Minimum :");
        bst.minimum().displayNode();

    }


}
