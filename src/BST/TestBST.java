package BST;

/**
 * Created by letsboogey on 13/02/2016.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static org.junit.Assert.*;

public class TestBST {

    //setup variables to capture console output
    private final PrintStream stdout = System.out;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    //initialise empty BST
    BinarySearchTree<Integer> bst = new BinarySearchTree<>();

    @Before
    public void initialise() throws UnsupportedEncodingException {

        System.setOut(new PrintStream(output, true, "UTF-8"));


        int[] keys = {12,34,78,98,10,7,6,11};
        for(int i : keys){
            bst.insert(i, 0);
        }
    }


    @Test
    public void testInsert(){
        //'out' is the output expected on the console
        String out = "{12, 0}Inserted at ROOT\n"+
                "{34, 0}inserted at right of:  12\n"+
                "{78, 0}inserted at right of:  34\n"+
                "{98, 0}inserted at right of:  78\n"+
                "{10, 0}inserted at left of:  12\n"+
                "{7, 0}inserted at left of:  10\n"+
                "{6, 0}inserted at left of:  7\n"+
                "{11, 0}inserted at right of:  10\n"
                ;
        assertEquals(out, output.toString());
    }

    @Test
    public void testFind1(){//existing key
        BinarySearchTree.Node found = bst.find(12);
        assertEquals(12,found.key);
    }

    @Test
    public void testFind2(){//missing key
        assertEquals(null,bst.find(100));
    }

    @Test
    public void testFind3(){//empty tree
        BinarySearchTree<Integer> empty = new BinarySearchTree<>();
        assertEquals(null,empty.find(9));
    }

    //redirect output back to stdout, so later print statements don’t go missing
    @After
    public void cleanUp() {
        System.setOut(stdout);
    }




}
