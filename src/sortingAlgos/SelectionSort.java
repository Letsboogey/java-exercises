package sortingAlgos;

/**
 * Created by letsboogey on 11/03/2016.
 */
public class SelectionSort {
    //O(n^2) in-place comparison sort algorithm
    //performs better than bubble but worse than insertion sort

    private static void selection_sort(int[] arr){
        int arr_size = arr.length;

        for(int i = 0 ; i < arr_size-1 ; i++){
            for(int j = i+1 ; j < arr_size ; j++){
                if(arr[j] < arr[i]){
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
            printArray(arr);
        }
    }

    //helper method to print progress
    public static void printArray(int[] arr){
        System.out.print("[ ");
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i]);
            if(i + 1 < arr.length){
                System.out.print(" , ");
            }
        }
        System.out.print(" ]\n");

    }

    public static void main(String[] args) {
        int[] array = new int[]{5,1,12,-5,16,2,12,14};
        selection_sort(array);

    }

}
