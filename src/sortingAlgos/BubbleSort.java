package sortingAlgos;

/**
 * Created by letsboogey on 11/03/2016.
 */
public class BubbleSort {
    //O(n^2) comparison sort algorithm
    //simple to implement but not practical for large N
    //smaller values bubble to the top of the list

    //logic to sort elements
    public static void bubble_sort(int[] arr){
        int arr_size = arr.length;
        int k;
        for(int i = 0 ; i < arr_size ; i++){
            for(int j = 0 ; j < arr_size -1 ; j++){
                k = j+1;
                if(arr[j] > arr[k]){//swap if left element > right element
                    int temp = arr[j];
                    arr[j] = arr[k];
                    arr[k] = temp;
                }
            }
            printArray(arr);
        }

    }

    //helper method to print progress
    public static void printArray(int[] arr){
        System.out.print("[ ");
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i]);
            if(i + 1 < arr.length){
                System.out.print(" , ");
            }
        }
        System.out.print(" ]\n");

    }

    public static void main(String[] args) {
        int[] array = new int[]{2,4,6,9,12,23,0,1,34};
        bubble_sort(array);

    }

}
