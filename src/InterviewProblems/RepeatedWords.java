package InterviewProblems;

/**
 * Created by letsboogey on 12/03/2016.
 * Write a program to read words from a file.
 * Count the repeated or duplicated words.
 * Sort it by maximum repeated words
 *
 * My solution:
 *
 * Read contents from file and add to hashmap
 * key is the word and value is count
 * read each map entry to a list interface (used arraylist), this is for the purpose of using the Collections.sort
 * override comparator for the sort
 */

import java.util.*;
import java.io.*;

public class RepeatedWords {

    //read file contents into a hashmap,maintaining word count
    private static HashMap<String,Integer> wordCount(File inputFile){
        FileInputStream fis;
        DataInputStream dis;
        BufferedReader br;
        HashMap<String,Integer> wordsMap = new HashMap<>();
        try{
            fis = new FileInputStream(inputFile);
            dis = new DataInputStream(fis);
            br = new BufferedReader(new InputStreamReader(dis));
            String line = null;
            while((line = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(line.replaceAll("[^a-zA-Z ]","")," ");
                while(st.hasMoreTokens()){
                    String word = st.nextToken().toLowerCase();
                    if(wordsMap.containsKey(word)){
                        wordsMap.put(word,wordsMap.get(word)+1);
                    }else{
                        wordsMap.put(word,1);
                    }
                }

            }

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return wordsMap;
    }

    private static void sortWords(HashMap<String,Integer> hm){

        Set<Map.Entry<String,Integer>> entrySet = hm.entrySet();
        List<Map.Entry<String,Integer>> list = new ArrayList(entrySet);


        Collections.sort(list, new Comparator<Map.Entry<String,Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        for(Map.Entry entry : list){
            System.out.println(entry.getKey() +" : "+entry.getValue() );
        }
    }

    public static void main(String[] args) {
        sortWords(wordCount(new File("/Users/letsboogey/inputFile.txt")));
    }
}
