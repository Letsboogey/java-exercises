package InterviewProblems;



import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;

public class FindDuplicates {

    private static void findfirstDuplicate(int[] inputArr){

        HashSet<Integer> hs = new HashSet<>();
        String duplicate = "No duplicates found";
        for(int i : inputArr){
            if(!hs.add(i)){
                duplicate = "Duplicate number: "+i;
                break;//halts function as soon as first duplicate is found, without it, last duplicate will be printed
            }
        }
        System.out.println(duplicate);
    }

    private static void findAllDuplicates(int[] inputArr){
        HashSet<Integer> hs = new HashSet<>();
        int numOfDuplicates = 0;
        for(int i : inputArr){
            if(!hs.add(i)){
                System.out.println( "Duplicate number found: "+i);
                numOfDuplicates++;
            }
        }
        System.out.println("Total number of duplicates = " + numOfDuplicates);
    }

    private static void printNumOfOccurences(int[] inputArr){
        HashMap<Integer,Integer> hm = new HashMap<>();
        for(int i : inputArr){
            if(hm.containsKey(i)){
                hm.put(i,hm.get(i)+1);
            }else{
                hm.put(i,1);
            }
        }
        for(Map.Entry entry : hm.entrySet()){
            System.out.println("["+entry.getKey()+"]" + " occurs "+ entry.getValue() + " times");
        }
    }

    public static void main(String[] args) {
        int[] myarray = {1,5,23,5,23,2,1,6,3,1,8,12,3,23};
        //findfirstDuplicate(myarray);
       // findAllDuplicates(myarray);
        printNumOfOccurences(myarray);
    }
}
