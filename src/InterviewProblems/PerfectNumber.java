package InterviewProblems;

/**
 * Created by letsboogey on 12/03/2016.
 *
 * A perfect number is a positive integer that is equal to the sum of its proper
 * positive divisors,that is , the sum of its positive divisors excluding the number itself.
 * Equivalently, a perfect number is a number that is half the sum of all its positive divisors including itself.
 * The first perfect number is 6, because 1+2+3 = 6...Equivalently (1+2+3+6)/2 = 6.
 * Write a program to check if a given number N is a perfect number.
 */
public class PerfectNumber {

    private static boolean isPerfectNumber(int N){

        int sum_of_divisors = 0;
        for(int i = 1; i <= N ; i++){
            if((N%i) == 0){
                sum_of_divisors += i;
            }
        }

        return (sum_of_divisors/2 == N);
    }

    public static void main(String[] args) {
        System.out.println(isPerfectNumber(28));
    }
}
