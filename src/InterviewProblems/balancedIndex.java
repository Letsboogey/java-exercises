package InterviewProblems;

/**
 * Created by letsboogey on 12/03/2016.
 *
 * You are given an array of numbers
 * Find out the array index where
 * Sum of numbers preceding index is equal to sum of numbers succeeding index
 *
 * My Solution:
 * initialise left_sum as number at index 0
 *compute total sum, subtract left_sum and number at starting index , and call it right_sum
 *iterate through list from left starting at index 1
 *subtract number at current index from right_sum and compare right_sum to left_sum
 *if they're equal, return current index
 * otherwise add number at current index to left_sum and check that next index exists
 * if it does, subtract number at next index from right_sum and repeat process
 */

public class balancedIndex {

    private static void findBalancedIndex(int[] inputArr){

        int total_sum = 0;
        boolean foundBalanceIndex = false;
        for(int i : inputArr){
            total_sum += i;
        }

        int left_sum = inputArr[0];
        int right_sum = total_sum - left_sum - inputArr[1];
        for(int k = 1 ; k < inputArr.length-1 ; k++){
            if(left_sum == right_sum){
                System.out.println("Balanced index at position : "+k);
                System.out.println("left_sum = " +left_sum + ": right_sum = "+right_sum);
                foundBalanceIndex = true;
                break;
            }else{
                right_sum -= inputArr[k+1];
                left_sum += inputArr[k];

            }
        }

        if(!foundBalanceIndex) System.out.println("Balanced index does not exist");

    }

    public static void main(String[] args) {
        int[] balanced1 = {2,2,2,2,2,2,2,2,2,2,2,2,2,2,900,2,2,2,2,2,2,2,2,2,2,2,2,2,2};
        int[] balanced2 = {30,38,15,15};
        int[] not_balanced = {2,4,4,5,4,1};

        findBalancedIndex(balanced2);
    }
    
    /*ALTERNATE SOLUTION I MADE ON A CODILITY DEMO TEST*/
    
    
    // you can also use imports, for example:
    // import java.util.*;

    // you can write to stdout for debugging purposes, e.g.
    // System.out.println("this is a debug message");


    public int solution(int[] A) {
        
        //corner case
        if(A.length < 2){
            return -1;
        }
        
        int current_index/*index 0 of input array, initially */, next_index;
        
        //sum of elements on left side of current index
        long left_sum = 0;
    
        //sum of elements on right side of current index
        long right_sum = 0;
        for(int i = 1 ; i < A.length ; i++){
            right_sum += A[i];
        }
        
        /*
        sample input array A = [-1,3,-4,5,1,-6,2,1]
        so currently we have that:
        current index = -1
        sum of elements at left of current index, left_sum = 0
        sum of elements on right of current index = 2
        
        left_sum != right_sum so current index is not equilibrium
        LOGIC: loop through input array, updating the current index and the left and right sums
        to look for the point where left_sum == right_sum
        */
        
        for(int j = 0 ; j < A.length-1 ; j++){
            current_index = A[j];
            next_index = A[j+1];
            if(left_sum == right_sum){
                return current_index;    
            }else{
                left_sum += current_index;
                right_sum -= next_index;
                
            }       
            
        }
        
        return -1;//if no equilibrium found
    }



}
