package InterviewProblems;

/**
 * Created by letsboogey on 12/03/2016.
 *
 * Write a program to convert decimal number to binary
 *
 * My solution:
 * Made assumption that number is positive
 * Used stringbuilder because I wanted to utilise the reverse method
 *
 */
public class DecimalToBinary {

    private static void decToBin(int n){
        StringBuilder binary = new StringBuilder();
        while(n > 0){
            binary.append(n%2);
            n = n/2;
        }
        System.out.println(binary.reverse());
    }

    public static void main(String[] args) {
        decToBin(1);
    }
}
