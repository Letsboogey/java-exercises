package InterviewProblems;

/**
 * Created by letsboogey on 13/03/2016.
 *
 * Write program to count repeated characters in a string and print their repetition count
 */

import java.util.HashMap;
import java.util.Map;

public class CountCharDuplicates {

    private static void countRepeatedChars(String inputStr){

        HashMap<Character,Integer> characterMap = new HashMap<>();
        char[] chars = inputStr.toCharArray();
        for(char ch : chars){
            if(characterMap.containsKey(ch)){
                characterMap.put(ch,characterMap.get(ch)+1);
            }else{
                characterMap.put(ch,1);
            }
        }

        for(Map.Entry entry : characterMap.entrySet()){
            if((int)entry.getValue() > 1) {

                System.out.println(entry.getKey() + " : " + entry.getValue());
            }
        }

    }

    public static void main(String[] args) {
        countRepeatedChars("panasonic");
    }

}
