package InterviewProblems;

/**
 * Created by letsboogey on 13/03/2016.
 *
 * Write a program to print the Fibonacci series
 */
public class FibonacciNumbers {

    //get Nth fibonacci number recursively
    private static long fibonacci(int Nth) {
        if (Nth == 0) {
            return 0;
        }else{
            return (Nth <= 2) ? 1 : fibonacci(Nth - 1) + fibonacci(Nth - 2);
        }
    }

    public void printFiboSeries(int limit){
        for(int i = 0 ; i <limit ; i++){
            System.out.print(fibonacci(i)+" ");
        }
    }


    public static void main(String[] args) {
        FibonacciNumbers fibs = new FibonacciNumbers();
        fibs.printFiboSeries(50);
    }


}
