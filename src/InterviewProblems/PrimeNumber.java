package InterviewProblems;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by letsboogey on 13/03/2016.
 *
 * program to check whether a given number is a prime number
 */
public class PrimeNumber {

    private static boolean isPrime(int num){

        if(num <= 1){
            return false;
        }else{
            for(int i=2 ; i < num ; i++){
                if(num%i == 0){
                    return false;
                }
            }
        }
        return  true;
    }


    public static void main(String[] args) {
        HashMap<Integer,Boolean> map = new HashMap<>();
        for(int j = 0 ; j<30 ; j++){
            map.put(j,isPrime(j));
        }

        for(Map.Entry entry : map.entrySet()){
            if((boolean)entry.getValue())
                System.out.println(entry.getKey() +" --> "+entry.getValue());
        }

    }
}
