package InterviewProblems;

/**
 * Created by letsboogey on 13/03/2016.
 *
 * Write a program to order a HashMap or any map data structure by value and by key.
 *
 * The solution:
 * Define a set to store the entries, this can be used to copy the entries to an arraylist
 * Collections.sort(arraylist, comparator) can be applied to the arraylist data structure
 * override the comparators compare method to sort by value of the entries
 */

import java.util.*;
public class SortMap {

    private static void sortMapByValue(HashMap map){

        //getting the map entries into a set
        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();

        //creating an arraylist of entries
        List<Map.Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(entrySet);

        Collections.sort(entryList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());//ascending
                //return o2.getValue().compareTo(o1.getValue());//descending
            }
        });

        for(Map.Entry entry : entryList){
            System.out.println(entry.getKey() +" --> "+ entry.getValue());
        }
    }

    //just for fun, sorting by key using a treeMap
    private static void sortByKey(HashMap map){
        TreeMap<String,Integer> tm = new TreeMap<>(map);
        for(Map.Entry entry : tm.entrySet()){
            System.out.println(entry.getKey() +" --> "+ entry.getValue());
        }
    }


    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("one",new Integer(10));
        map.put("two",new Integer(13));
        map.put("three",new Integer(53));
        map.put("four",new Integer(5));
        map.put("foo",new Integer(503));
        map.put("bar",new Integer(3));
        map.put("foobar",new Integer(39));
        map.put("tee",new Integer(0));
        map.put("seven",new Integer(12));

        //sortMapByValue(map);
        sortByKey(map);
    }
}
