package InterviewProblems;

/**
 * Created by letsboogey on 12/03/2016.
 *
 * Write a program to reverse a string recursively. Do not use any string reversal methods from the java library
 *
 * My solution:
 *
 * A recursive method is one that calls itself repeatedly to achieve a task
 * so my method calls itself repeatedly to print the last letter of a string
 * it is first called on the full string
 * then inside it calls itself on the string minus the last letter which is already printed
 *
 */
public class ReverseString {

    private static void reverseRecursive(String str) {
        int strlen = str.length();
        if (!str.isEmpty()) {
            System.out.print(str.substring(strlen - 1, strlen));
            reverseRecursive(str.substring(0, strlen - 1));
        }
    }
    public static void main(String[] args) {
        String[] strings = {"I am a genius", "madam", "too easy", "poooooped","e"};
        for(String st : strings){
            reverseRecursive(st);
            System.out.println();
        }


    }
}
