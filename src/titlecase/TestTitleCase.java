package titlecase;

import org.junit.*;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by letsboogey on 06/02/2016.
 */
public class TestTitleCase {

    @Test
    public void cornerCase1(){
        String single_word = "tHe";
        assertEquals("The", TitleCase.titleCase(single_word));
    }

    @Test
    public void cornerCase2(){
        String empty = "";
        assertEquals("",TitleCase.titleCase(empty));
    }

    @Test
    public void example1(){
        String fancycase = "i Am goIng To nAIl thIs INTERVIEW";
        assertEquals("I Am Going to Nail This Interview", TitleCase.titleCase(fancycase));
    }

    @Test
    public void example2(){
        String allLowerCase = "i am going to nail this interview";
        assertEquals("I Am Going to Nail This Interview",TitleCase.titleCase(allLowerCase));
    }

    @Test
    public void example3(){
        String allUpperCase = "I AM GOING TO NAIL THIS INTERVIEW";
        assertEquals("I Am Going to Nail This Interview",TitleCase.titleCase(allUpperCase));
    }

}
