package titlecase;

import java.util.Arrays;
import java.util.List;

public class TitleCase {

    private static List<String> connector_words = Arrays.asList("to","and","a","the","at","in","with","but","or");

    //converts a given string to titlecase
    public static String titleCase(String title){
        //corner case for empty string
        if(title.isEmpty())
            return "";

        title = title.toLowerCase();
        String[] tokens = title.split(" ");

        tokens[0] = Capitalize(tokens[0]);
        tokens[tokens.length-1] =  Capitalize(tokens[tokens.length-1]);

        for(int i=1 ; i<tokens.length-1 ; i++){
            if(!connector_words.contains(tokens[i])) {
               tokens[i] = Capitalize(tokens[i]);
            }
        }
        return String.join(" ",tokens);
    }

    public static String Capitalize(String word){
        word = word.toLowerCase();
        String capitalized_word = word.substring(0,1).toUpperCase() + word.substring(1);
        return capitalized_word;
    }

}
